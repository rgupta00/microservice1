package com.empappclient.client;

import java.util.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.RestTemplate;

@RestController
public class EmployeeClientUsingGetForObject {

	@Autowired
	private RestTemplate restTemplate;

//	@GetMapping("client")
//	public String client() {
//		//how to call one api from another project=> restTemplate
//		//3 kind of methods 
//		//exchange--------- getForEntity---------getForObject
//		
//		Employee employee=restTemplate
//				.getForObject("http://localhost:8080/empapp/employee/1",Employee.class);
//		
//		return employee.toString();
//	}

//	@GetMapping("client")
//	public String client() {
//		//how to call one api from another project=> restTemplate
//		//3 kind of methods 
//		//exchange--------- getForEntity---------getForObject
//		
//		String uri="http://localhost:8080/empapp/employee/{id}";
//		
//		Map<String, String>map=new HashMap<>();
//		map.put("id", "2");
//		
//		Employee employee=restTemplate
//				.getForObject(uri, Employee.class, map);
//		
//		return employee.toString();
//	}

//	//get response as String
//	@GetMapping("client")
//	public String client() {
//		//how to call one api from another project=> restTemplate
//		//3 kind of methods 
//		//exchange--------- getForEntity---------getForObject
//		
//		String uri="http://localhost:8080/empapp/employee/{id}";
//		
//		Map<String, String>map=new HashMap<>();
//		map.put("id", "2");
//		
//		String response=restTemplate
//				.getForObject(uri, String.class, map);
//		
//		return response;
//	}

//	//get all emp as collection
//		@GetMapping("client")
//		public List client() {
//			//how to call one api from another project=> restTemplate
//			//3 kind of methods 
//			//exchange--------- getForEntity---------getForObject
//			
//			String uri="http://localhost:8080/empapp/employee";
//			
//			
//			List empList=restTemplate
//					.getForObject(uri, List.class);
//			
//			return empList;
//		}

	// post an new employee from remove api
//	@GetMapping("add-client")
//	public Employee client() {
//
//		Employee e = new Employee("suman", 40);
//		Employee empSaved = restTemplate.postForObject("http://localhost:8080/empapp/employee", e, Employee.class);
//
//		return empSaved;
//	}

}
