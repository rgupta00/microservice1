package com.empappclient.client;

import java.util.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.RestTemplate;

@RestController
public class EmployeeClientUsingExchange {

	@Autowired
	private RestTemplate restTemplate;

	
	//get rec using exchange method 
	@GetMapping("get-an-client")
	public Employee client() {
		String url="http://localhost:8080/empapp/employee/4";
		HttpHeaders headers=new HttpHeaders();
		headers.setContentType(MediaType.APPLICATION_JSON);
		
		HttpEntity<Object> entity=new HttpEntity<Object>(headers);
		
		ResponseEntity<Employee> resp = 
				restTemplate.exchange(url,
				HttpMethod.GET, entity, Employee.class);
		
		Employee employee=resp.getBody();
		System.out.println(resp.getHeaders());
		System.out.println(resp.getStatusCode());
		
		return employee;
	}
	
	
}




